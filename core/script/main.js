/* ##### Variables Pages Section ##### */
const startPage = document.querySelector("#start");
const quizPage = document.querySelector("#quiz");
const endPage = document.querySelector("#end");


/* ##### Button Start Function ##### */
let startBtn = document.querySelector(".btn-start");

startBtn.addEventListener("click", function(){
    startQuiz();
})


function startQuiz() {
    let errorName = document.querySelector(".error-name")
    let letters =/^[a-z, A-Z]*$/;


    if (userName.value.length == 0) {
        errorName.innerHTML = `Error! name must not be empty.`;

    } else if (userName.value.length < 4 ) {
        errorName.innerHTML = `Error! name must be longer than 3 characters.`;

    } else if  (userName.value.length > 21 ) {
        errorName.innerHTML = `Error! name must be shorter than 20 characters.`;

    } else if (!userName.value.match(letters)) {
        errorName.innerHTML = `Error! name must be letters only.`;
    }
    else {
        startPage.style.display = "none";
        quizPage.style.display = "flex";
    }
}


/* ##### JSON Static Object ##### */
let questionsData = [
    {   
        number: "1",
        question: "Sapi adalah termasuk hewan...",
        answer: {
            a: "Omnivora",
            b: "Carnivora",
            c: "Herbivora"
            },
            correctAnswer: "c"
        },
    {
        number: "2",
        question: "Harimau adalah termasuk hewan...",
        answer: {
            a: "Omnivora",
            b: "Carnivora",
            c: "Herbivora"
            },
            correctAnswer: "b"
        },
    {
        number: "3",
        question: "Persatuan Indonesia adalah bunyi sila ke...",
        answer: {
            a: "Satu",
            b: "Dua",
            c: "Tiga",
            },
            correctAnswer: "c"
        },
    {
        number: "4",
        question: "Hari kemerdekaan Indonesia jatuh pada tanggal...",
        answer: {
            a: "16 Agustus",
            b: "17 Agustus",
            c: "18 Agustus"
            },
            correctAnswer: "b"
        },
    {
        number: "5",
        question: "Ikan bernafas dengan...",
        answer: {
            a: "Insang",
            b: "Paru-paru",
            c: "Jantung"
            },
            correctAnswer: "a"
        },
    {
        number: "6",
        question: "Nama Bandara di Yogyakarta adalah...",
        answer: {
            a: "Ahmad Yani",
            b: "Adi Sumarmo",
            c: "Adi Sucipto"
            },
            correctAnswer: "c"
        },
    {
        number: "7",
        question: "Ibukota Propinsi Kalimantan Barat adalah...",
        answer: {
            a: "Palangkaraya",
            b: "Balikpapan",
            c: "Pontianak"
            },
            correctAnswer: "c"
        },
    {
        number: "8",
        question: "Simbiosis yang saling menguntungkan disebut...",
        answer: {
            a: "Komensalisme",
            b: "Parasitisme",
            c: "Mutualisme"
            },
            correctAnswer: "c"
        },
    {
        number: "9",
        question: "Berapa jumlah benua di dunia?",
        answer: {
            a: "4",
            b: "5",
            c: "6"
            },
            correctAnswer: "b"
        },
    {
        number: "10",
        question: "Makanan khas Jawa Timur adalah...",
        answer: {
            a: "Rendang",
            b: "Gudeg",
            c: "Rawon"
            },
            correctAnswer: "c"
        },
];


/* ##### Looping Quiz ##### */
let quizPageHTML = "";

questionsData.forEach((question, index) => {
    if (index === 0){
        quizPageHTML += `
        <div id="list" class="question-page">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}<i class="bi bi-caret-right-fill"></i></h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c} <i class="bi bi-check-lg"></i></label>
                </div>
                <div class="question-page__question--button">
                    <button id="next" class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else if (index <= 8 ){
        quizPageHTML += `
        <div id="list" class="question-page" style="display:none">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}<i class="bi bi-caret-right-fill"></i></h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c} <i class="bi bi-check-lg"></i></label>
                </div>
                <div class="question-page__question--button">
                    <button id="prev" class="btn-prev">Prev</button>
                    <button id="next" class="btn-next">Next</button>
                </div>
            </div>
        </div>`
    } else {
        quizPageHTML += `
        <div id="list" class="question-page" style="display:none">
            <div class="container">
                <div class="skills" style="width: ${question.number}0%"></div>
            </div>
            <div class="question-page__number">
                <h3 id="question-number">${question.number}<i class="bi bi-caret-right-fill"></i></h3>
            </div>
            <div class="question-page__question">
                <h2 class="question-page__question--text">${question.question}</h2>
                <div class="question-page__question--multiple">
                    <input type="radio" name="q-${question.number}" value="a" id="a${question.number}">
                    <label for="a${question.number}">${question.answer.a} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="b" id="b${question.number}">
                    <label for="b${question.number}">${question.answer.b} <i class="bi bi-check-lg"></i></label>
                    <input type="radio" name="q-${question.number}" value="c" id="c${question.number}">  
                    <label for="c${question.number}">${question.answer.c} <i class="bi bi-check-lg"></i></label>
                </div>
                <div class="question-page__question--button">
                    <button id="prev" class="btn-prev">Prev</button>
                    <button id="finish" class="btn-finish">Finish</button>
                </div>
            </div>

            <div id="my-modal" class="modal">
                <div class="modal__content">
                    <h2 class="modal__content--title">ATTENTION !</h2>
                    <p class="modal__content--attention">"You must answer all the questions. Check Again!"</p>
                    <i id="x-modal" class="bi bi-x-square-fill modal__content--close"></i>
                </div>
            </div>
        </div>`
    }
});

quizPage.innerHTML = quizPageHTML;



/* ##### Button Next Function ##### */
let nextBtn = document.querySelectorAll(".btn-next");
nextBtn.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        
        let quizes = e.target.parentElement.parentElement.parentElement
        quizes.setAttribute("style", "display:none;")

        let quizesSibling = quizes.nextElementSibling
        quizesSibling.removeAttribute("style")
    })
})


/* ##### Button Previous Function ##### */
let prevBtn = document.querySelectorAll(".btn-prev");
prevBtn.forEach((btn) => {
    btn.addEventListener("click", (e) => {
        let quizes = e.target.parentElement.parentElement.parentElement
        quizes.setAttribute("style", "display:none;")

        let quizesSibling = quizes.previousElementSibling
        quizesSibling.removeAttribute("style")
    })
})


/* ##### Button Finish Function ##### */
let finishBtn = document.querySelector(".btn-finish");
finishBtn.addEventListener("click", function() {

        scoring();
});


/* ##### Looping Scoring ##### */
function scoring() {
    let answered = []
    let checkQuiz = [
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    ]
    let openModal = document.querySelector("#my-modal")
    let closeModal = document.querySelector("#x-modal")

    
    for (i=1; i <= checkQuiz.length; i++){  
        let userAnswered = document.querySelector(`input[name=q-${i}]:checked`);
        if (userAnswered){
            answered.push(userAnswered.value);
        }
    }
    
    if (answered.length < checkQuiz.length) {
        openModal.style.display = "block";
    } else {
        quizContainer.style.display = "none";
        resultContainer.style.display = "flex";
    } 

    closeModal.addEventListener("click", () => {
        openModal.style.display = "none";
    })

    let score = 0;    
    answered.forEach((answers, index) =>{
        
        if(answers === questionData[index].correctAnswer){
            score +=1;
        }
        
    });

    correctAnswer(score);
}


/* ##### Result Section ##### */
let userName = document.querySelector(".user-name");

function correctAnswer(score) {
    let endPageHTML = "";

    if (score < 7) {
        endPageHTML += `
        <h2 class="result-page__person"><i class="bi bi-person-badge"></i><i class="bi bi-person-workspace"></i> ${userName.value}</h2>
        <h3 class="result-page__title">Thanks for taking this Quiz!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="first">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="first">You are below avergae!</p>
        </div>
        <button class="result-page__again btn-again">Start Over!</button>`
    } else if (score < 10) {
        endPageHTML += `
        <h2 class="result-page__person"><i class="bi bi-person-badge"></i><i class="bi bi-person-workspace"></i> ${userName.value}</h2>
        <h3 class="result-page__title">Thanks for taking this Quiz!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="second">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="second">You are smart enough!</p>
        </div>
        <button class="result-page__again btn-again">Start Over!</button>`
    } else {
        endPageHTML += `
        <h2 class="result-page__person"><i class="bi bi-person-badge"></i><i class="bi bi-person-workspace"></i> ${userName.value}</h2>
        <h3 class="result-page__title">Thanks for taking this Quiz!</h3>
        <h4 class="result-page__result">Result :</h4>
        <div id="score-number" class="result-page__scorenumber">
            <h4 class="third">${score} / 10</h4>
        </div>
        <div id="score-text" class="result-page__scoretext">
            <p class="third">You're Genius!</p>
        </div>
        <button class="result-page__again btn-again">Start Over!</button>`
    } 

    endPage.innerHTML = endPageHTML;


    let againBtn = document.querySelector(".btn-again");

    againBtn.addEventListener("click", function() {
        window.location.reload();
    });
};